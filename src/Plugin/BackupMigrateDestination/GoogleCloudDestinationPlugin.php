<?php

namespace Drupal\backup_migrate_google_cloud\Plugin\BackupMigrateDestination;

use Drupal\backup_migrate\Drupal\EntityPlugins\DestinationPluginBase;

/**
 * Defines a Google Cloud destination plugin.
 *
 * @BackupMigrateDestinationPlugin(
 *   id = "google_cloud",
 *   title = @Translation("Google Cloud"),
 *   description = @Translation("Backup to Google Cloud storage."),
 *   wrapped_class = "\Drupal\backup_migrate_google_cloud\Destination\GoogleCloudDestination"
 * )
 *
 * Note: It is unclear why the plugin is constructed with wrapped_class. To keep
 * things consistent, the deprecated Psr4ClassLoader is called in
 * backup_migrate_google_cloud.module.
 *
 * @see \Drupal\backup_migrate\Drupal\EntityPlugins\WrapperPluginBase::getObject()
 */
class GoogleCloudDestinationPlugin extends DestinationPluginBase {
}
