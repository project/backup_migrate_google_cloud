<?php

namespace Drupal\backup_migrate_google_cloud\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Google Cloud Backup & Migrate Route Subscriber.
 */
class GoogleCloudRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.backup_migrate_destination.backup_download')) {
      $route->setDefaults([
        '_controller' => '\Drupal\backup_migrate_google_cloud\Controller\GoogleCloudBackupController::download',
      ]);
    }
  }

}
