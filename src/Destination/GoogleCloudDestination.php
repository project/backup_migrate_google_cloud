<?php

namespace Drupal\backup_migrate_google_cloud\Destination;

use Google\Cloud\Core\Exception\ServiceException;
use Google\Cloud\Storage\Bucket;
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Storage\StorageObject;

use Drupal\backup_migrate\Core\Config\ConfigInterface;
use Drupal\backup_migrate\Core\Config\ConfigurableInterface;
use Drupal\backup_migrate\Core\Exception\BackupMigrateException;
use Drupal\backup_migrate\Core\Destination\RemoteDestinationInterface;
use Drupal\backup_migrate\Core\Destination\ListableDestinationInterface;
use Drupal\backup_migrate\Core\File\BackupFile;
use Drupal\backup_migrate\Core\Destination\ReadableDestinationInterface;
use Drupal\backup_migrate\Core\File\BackupFileInterface;
use Drupal\backup_migrate\Core\File\BackupFileReadableInterface;
use Drupal\backup_migrate\Core\Destination\DestinationBase;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;

use Symfony\Component\HttpFoundation\Response;

/**
 * Google Cloud Backup & Migrate Destination.
 *
 * @package Drupal\backup_migrate_google_cloud\Destination
 */
class GoogleCloudDestination extends DestinationBase implements RemoteDestinationInterface, ListableDestinationInterface, ReadableDestinationInterface, ConfigurableInterface {

  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * Google Cloud Storage Client.
   *
   * @var \Google\Cloud\Storage\StorageClient
   */
  protected $client = NULL;

  /**
   * Google Cloud Storage Bucket.
   *
   * @var \Google\Cloud\Storage\Bucket
   */
  protected $bucket = NULL;

  /**
   * Key repository service.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository = NULL;

  /**
   * File repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository = NULL;

  /**
   * Filesystem service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigInterface $config) {
    parent::__construct($config);

    /** @codingStandardsIgnoreStart */

    /**
     * @var \Drupal\key\KeyRepository keyRepository
     */
    $this->keyRepository = \Drupal::service('key.repository');

    /**
     * @var \Drupal\file\FileRepository fileRepository
     */
    $this->fileRepository = \Drupal::service('file.repository');

    /**
     * @var \Drupal\Core\File\FileSystem fileSystem
     */
    $this->fileSystem = \Drupal::service('file_system');

    /** @codingStandardsIgnoreEnd */
  }

  /**
   * Download backup file.
   *
   * @param \Drupal\backup_migrate\Core\File\BackupFileInterface $file
   *   Backup file.
   *
   * @return \Symfony\Component\HttpFoundation\Response|void
   *   Returns http file response.
   */
  public function downloadFile(BackupFileInterface $file) {
    $id = $file->getMeta('id');
    if ($this->fileExists($id)) {
      try {
        $object = $this->getStorageBucket()->object($id);
        if ($object) {
          $info = $object->info();
          return new Response(
            $object->downloadAsStream(),
            Response::HTTP_OK,
            ['content-type' => $info['contentType']]
          );
        }
        else {
          $this->getLogger('backup_migrate_google_cloud')
            ->warning('Object was not found. Unable to download backup file.');
        }
      } catch (ServiceException $e) {
        watchdog_exception('backup_migrate_google_cloud - downloadFile()', $e);
      }
    }
    else {
      $this->getLogger('backup_migrate_google_cloud')
        ->warning('Backup file was not found. Unable to download backup file.');
    }
  }

  /**
   * Get bucket name.
   *
   * @return mixed
   */
  private function getBucketName() {
    return $this->confGet('gc_bucket');
  }

  /**
   * Get credentials file.
   *
   * @return mixed|string|null
   */
  private function getCredentialsFile() {
    $credentialsFile = NULL;
    if ($credentialsKeyName = $this->confGet('gc_credentials_key_name')) {
      $credentialsFile = $this->keyRepository->getKey($credentialsKeyName)->getKeyValue();
    }
    return $credentialsFile;
  }

  /**
   * Get directory/prefix.
   *
   * @return mixed
   */
  private function getPrefix() {
    return $this->confGet('gc_prefix');
  }

  /**
   * Get Google Cloud Storage Client.
   *
   * @return \Google\Cloud\Storage\StorageClient|null
   */
  public function getStorageClient(): ?StorageClient {
    // Check client.
    if ($this->client == NULL) {

      // Get bucket.
      $bucket = $this->getBucketName();
      if (empty($bucket)) {
        $this->getLogger('backup_migrate_google_cloud')
          ->warning('Bucket name not configured.');
        return NULL;
      }

      // Get credentials
      $credentials = $this->getCredentialsFile();
      if (empty($credentials)) {
        $this->getLogger('backup_migrate_google_cloud')
          ->warning('Credentials not configured.');
        return NULL;
      }

      $this->client = new StorageClient([
        'keyFile' => json_decode($credentials, true),
      ]);

      // Set current bucket.
      $this->bucket = $this->client->bucket($bucket);
    }

    // Return client.
    return $this->client;
  }

  /**
   * Get Google Cloud Storage Bucket.
   *
   * @return \Google\Cloud\Storage\Bucket|null
   */
  public function getStorageBucket(): ?Bucket {
    // Check bucket.
    if ($this->bucket == NULL) {
      // Make sure we have client.
      if ($this->client = $this->getStorageClient()) {
        // Get bucket.
        $bucket = $this->getBucketName();
        if (empty($bucket)) {
          $this->getLogger('backup_migrate_google_cloud')
            ->warning('Bucket name not configured.');
          return NULL;
        }
        // Set current bucket.
        $this->bucket = $this->client->bucket($bucket);
      }
    }
    return $this->bucket;
  }

  /**
   * {@inheritdoc}
   */
  public function checkWritable(): bool {
    return TRUE;
  }

  /**
   * @inheritDoc
   */
  protected function deleteTheFile($id) {
    if ($this->fileExists($id)) {
      try {
        $object = $this->getStorageBucket()->object($id);
        $object->delete();
      } catch (ServiceException $e) {
        watchdog_exception('backup_migrate_google_cloud - deleteTheFile()', $e);
      }
    } else {
      $this->getLogger('backup_migrate_google_cloud')->warning('Unable to delete backup file. File was not found.');
    }
  }

  /**
   * @inheritDoc
   *
   * @throws \Drupal\backup_migrate\Core\Exception\BackupMigrateException
   */
  protected function saveTheFile(BackupFileReadableInterface $file): ?StorageObject {
    $object = NULL;
    if (file_exists($file->realpath())) {
      try {
        $object = $this->getStorageBucket()->upload($file->openForRead(), [
          'name' => $file->getFullName(),
        ]);
        $object->update(['acl' => []], ['predefinedAcl' => 'PUBLICREAD']);
      } catch (ServiceException $e) {
        $this->getLogger('backup_migrate_google_cloud')->error($e->getMessage());
        throw new BackupMigrateException('Could not upload to Google Cloud: %err (code: %code)', [
          '%err' => $e->getMessage(),
          '%code' => $e->getCode(),
        ]);
      }
    }
    else {
      $this->getLogger('backup_migrate_google_cloud')->warning('Backup file was not found.');
    }
    return $object;
  }

  /**
   * @inheritDoc
   */
  protected function saveTheFileMetadata(BackupFileInterface $file) {
    // Nothing to do here.
  }

  /**
   * @inheritDoc
   */
  protected function loadFileMetadataArray(BackupFileInterface $file) {
    // Nothing to do here.
  }

  /**
   * @inheritDoc
   */
  public function listFiles() {
    $files = [];

    try {
      $options = [];
      if ($prefix = $this->getPrefix()) {
        $options['prefix'] = $prefix;
      }
      /** @var StorageObject $object */
      foreach ($this->getStorageBucket()->objects($options) as $object) {
        // Object metadata.
        $info = $object->info();

        // Setup new backup file.
        $backupFile = new BackupFile();
        $backupFile->setMeta('id', $object->name());
        if (isset($info['size'])) {
          $backupFile->setMeta('filesize', $info['size']);
        }
        if (isset($info['updated'])) {
          $backupFile->setMeta('datestamp', strtotime($info['updated']));
        }
        $backupFile->setFullName($object->name());
        $backupFile->setMeta('metadata_loaded', TRUE);

        // Add backup file to array.
        $files[$object->name()] = $backupFile;
      }

    } catch (ServiceException $e) {
      watchdog_exception('backup_migrate_google_cloud - listFiles()', $e);
    }

    return $files;
  }

  /**
   * @inheritDoc
   */
  public function queryFiles(array $filters = [], $sort = 'datestamp', $sort_direction = SORT_DESC, $count = 100, $start = 0) {
    // Get the full list of files.
    $out = $this->listFiles($count + $start);
    foreach ($out as $key => $file) {
      $out[$key] = $this->loadFileMetadata($file);
    }
    // Filter the output.
    $out = array_reverse($out);
    // Slice the return array.
    if ($count || $start) {
      $out = array_slice($out, $start, $count);
    }
    return $out;
  }

  /**
   * @inheritDoc
   */
  public function countFiles() {
    $file_list = $this->listFiles();
    return count($file_list);
  }

  /**
   * @inheritDoc
   */
  public function getFile($id) {
    $files = $this->listFiles();
    if (isset($files[$id])) {
      return $files[$id];
    }
    return NULL;
  }

  /**
   * @inheritDoc
   */
  public function loadFileForReading(BackupFileInterface $file) {
    // If this file is already readable, simply return it.
    if ($file instanceof BackupFileReadableInterface) {
      return $file;
    }
    $id = $file->getMeta('id');
    try {
      $object = $this->getStorageBucket()->object($id);
      $object->downloadAsStream();
    } catch (ServiceException $e) {
      watchdog_exception('backup_migrate_google_cloud - loadFileForReading()', $e);
    }
    return NULL;
  }

  /**
   * @inheritDoc
   */
  public function fileExists($id): bool {
    return (boolean) $this->getFile($id);
  }

  /**
   * Init configurations.
   *
   * @param array $params
   *
   * @return array
   */
  public function configSchema(array $params = []): array {
    $schema = [];

    // Init settings.
    if ($params['operation'] == 'initialize') {

      // Get available keys.
      $key_collection_url = Url::fromRoute('entity.key.collection')->toString();
      $keys = $this->keyRepository->getKeys();
      $key_options = [];
      foreach ($keys as $key_id => $key) {
        $key_options[$key_id] = $key->label();
      }

      // Credentials key.
      $schema['fields']['gc_credentials_key_name'] = [
        'type' => 'enum',
        'title' => $this->t('Credentials Key'),
        'description' => $this->t('Credentials key to use Google Cloud Storage. Use keys managed by the key module. <a href=":keys">Manage keys</a>', [
          ':keys' => $key_collection_url,
        ]),
        'empty_option' => $this->t('- Select Key -'),
        'options' => $key_options,
        'required' => TRUE,
      ];

      // Bucket name.
      $schema['fields']['gc_bucket'] = [
        'type' => 'text',
        'title' => $this->t('Bucket Name'),
        'required' => TRUE,
        'description' => $this->t('Name of bucket used in Google Cloud storage.'),
      ];

      // Folder prefix.
      $schema['fields']['gc_prefix'] = [
        'type' => 'text',
        'title' => $this->t('Sub-folder within the Google Cloud (optional)'),
        'required' => FALSE,
        'description' => $this->t('If you wish to organise your backups into a sub-folder such as /my/subfolder/, enter <i>my/subfolder</i> here without the leading or trailing slashes.'),
      ];
    }

    return $schema;
  }

}
